<?php

/**
 * Created by PhpStorm.
 * User: ntsybrik
 * Date: 9/13/16
 * Time: 7:01 PM
 */
class controller_message extends Controller
{
    public function action_send(){
        $message = model_message::receive_data();
       $response = model_message::save_data($message);

        if(isset($response)){
           //sends response to Curl
            echo $response;
       }
    }
}