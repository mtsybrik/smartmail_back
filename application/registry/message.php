<?php
/**
 * Created by PhpStorm.
 * User: ntsybrik
 * Date: 9/16/16
 * Time: 4:57 PM
 */

class Message
{
    private $name;
    private $email;
    private $message;

    public function __construct($messageName,$messageEmail,$messageMessage)
    {
        $this->name = $messageName;
        $this->email=$messageEmail;
        $this->message=$messageMessage;
    }

    public function getName()
    {
        return $this->name;
    }
    public function getEmail(){
        return $this->email;
    }
    public function getMessage(){
        return $this->message;
    }
}