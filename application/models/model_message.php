<?php
/**
 * Created by PhpStorm.
 * User: ntsybrik
 * Date: 9/13/16
 * Time: 10:30 PM
 */

class model_message extends Model
{
    public static function receive_data()
    {
        //extract data from the post
        $entityBody = file_get_contents('php://input');
        $message_body = json_decode($entityBody,true);

        //instantiate Message object and fill it with request data
        $message = new Message($message_body["name"],$message_body["email"],$message_body["message"]);
        return $message;
    }
    public static function save_data($message)
    {
        // Create connection
        $conn = new mysqli('localhost','root','root','smartmail_back','3307');
        // Check connection
        if ($conn->connect_error) {
            die("Connection failed: " . $conn->connect_error);
        }
        //generating GUID
        $guid=GUID::getGUID();

        //taking current datetime YYYY-MM-DDThh:mm:ss
        $datetime = date('Y-m-d\TH:i:s');

        $sql = "INSERT INTO messages (name, email, message, date_created , id)
        VALUES ('". $message->getName() . "', '" . $message->getEmail() . "', '" . $message->getMessage() . "', '". $datetime . "', '". $guid . "' )";

        //Set charset to support Russian
        $conn->set_charset("utf8");

        if ($conn->query($sql) === TRUE) {
            $conn->close();
            return "New record created successfully";
        } else {
            $conn->close();
            return "Error: " . $sql . "<br>" . $conn->error;
        }
    }
}